![](logo.jpg)

[![License: MIT](https://img.shields.io/github/license/Ileriayo/markdown-badges?style=for-the-badge)](https://opensource.org/licenses/MIT)[![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)](LICENSE)[![Selenium](https://img.shields.io/badge/Selenium-43B02A?style=for-the-badge&logo=Selenium&logoColor=white)](LICENSE)[![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)](LICENSE)[![Bootstarp](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)](LICENSE)[![Gmail](https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white)](LICENSE)[![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)](LICENSE)[![macOS](https://img.shields.io/badge/mac%20os-000000?style=for-the-badge&logo=macos&logoColor=F0F0F0)](LICENSE)[![Indeed](https://img.shields.io/badge/indeed-003A9B?style=for-the-badge&logo=indeed&logoColor=white)](LICENSE)[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](LICENSE)

## Description

This project was created just for fun. We spend a lot of time each day in the routine of job hunting. The typical workflow involves manual tasks like searching for suitable job openings on various job boards, checking if we've applied for some of them previously, and ensuring none of the potential employers from these listings are on our 'do not apply/stop' list. So, all these boring tasks are automated by building a BDD framework from scratch.

## Prerequisites

- Gmail is mandatory.
- Set up a dedicated label in your Gmail account to organize confirmation emails received after applying to job boards.
- Generate a file named `gmail_credentials.json` by following the instructions provided in the guide at https://developers.google.com/gmail/api/quickstart/python. This file should be moved to the project root folder.
- Create an ODS spreadsheet file for maintaining and updating your 'do not apply/stop' list. The first column of this spreadsheet should be reserved for company names.

## Installation

1. Clone the project
2. Install Python and all requirements
3. Put `gmail_credentials.json` to the project root folder
4. Set up Gmail label name and paths to ODS, HTML, and PDF files
5. Replace username and password with your own in `credentials.json` if you plan to use LinkedIn
6. Edit feature file as your need

## Usage example

##### Finding open positions on the job board

```
Feature: Looking For a Job

  Background: Set variables
    Given The path to ODS file is C:\my_jobs\log.ods
    And The path to HTML file folder is C:\my_jobs\ready_to_apply\
    And The path to PDF file folder is C:\my_jobs\ready_to_apply\
    And The label for emails in Gmail is label:_applied
    And The cover template is cover_letter_my.template

  Scenario: Find remote "Job of Dream" position on Indeed
    Given Indeed: Find "Job of Dream" in location "remote" within "any" miles for the last "3" days
    When Remove the positions that do not have "engineer" in the job title from the list of found on "Indeed"
    And Remove the positions that have "manager" in the job title from the list of found on "Indeed"
    And Gmail API: Remove the positions that have already been applied for from the list of found on "Indeed"
    And Docs API: Remove the positions that have been rejected from the list of found on "Indeed"
    Then Docs API: Create HTML file with the found positions on "Indeed"
    And Docs API: Open created HTML file in the browser
    
  Scenario: Find "Copywriter" position near Portland on LinkedIn
    Given LinkedIn: Find "Copywriter" in location "Portland" within "any" miles for the last "any" days
    When Remove the positions that do not have "copywriter" in the job title from the list of found on "LinkedIn"
    And Remove the positions that have "manager" in the job title from the list of found on "LinkedIn"
    And Gmail API: Remove the positions that have already been applied for from the list of found on "LinkedIn"
    And Docs API: Remove the positions that have been rejected from the list of found on "LinkedIn"
    Then Docs API: Create HTML file with the found positions on "LinkedIn"
    And Docs API: Open created HTML file in the browser  
```

##### Verifying if any applications were applied to the employer before

```
  Scenario: Verify if I have never applied for this position before
    When Looking for company "Employer of Dream"
    Then Gmail API: Verify if "company" should not exist in emails
    And Docs API: Verify if "company" should not exist in ODS file
```

##### Generate cover letter

```
  Scenario: Generate cover letter
    Given Doc API: Generate the cover letter for the position "Engineer" in company "The Company" for "Hiring Manager"
    And Docs API: Open created PDF file in the browser
```

##### Generate a report of confirmation emails

```
  Scenario: Generate report of confirmation emails
    Given Gmail API: Get a list of application confirmations from "any sources"
    When Docs API: Create HTML file with confirmations
    Then Docs API: Open created HTML file in the browser
```

