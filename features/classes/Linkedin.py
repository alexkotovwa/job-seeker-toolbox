from selenium.webdriver.common.by import By
from features.classes.Base import Base
from time import sleep


class Linkedin(Base):

    def scroll_down(self):
        elements = self.driver.find_elements(By.XPATH, "//ul[contains(@class, 'list-container')]/li")
        js_code = "arguments[0].scrollIntoView();"
        for i in range(len(elements)):
            self.driver.execute_script(js_code, elements[i])

    def get_jobs_on_page(self):
        result_content = self.driver.find_elements(
            By.XPATH, "//div[contains(@class, ' artdeco-entity-lockup__content ember-view')]")
        found = []
        for i in range(len(result_content)):
            job_title = result_content[i].find_element(By.XPATH, "./div/a").text
            job_href = result_content[i].find_element(By.XPATH, "./div/a").get_attribute('href')
            job_company = result_content[i].find_element(By.XPATH, "./div/span").text
            found.append([job_company, job_title, job_href])
        return found

    def get_jobs(self, credentials, title, location, radius, date_posted):

        url_linkedin = f"https://linkedin.com/"

        self.driver.get(url_linkedin)

        username = self.driver.find_element(By.XPATH, "//*[@id='session_key']")
        password = self.driver.find_element(By.XPATH, "//*[@id='session_password']")

        username.send_keys(credentials['linkedin']['username'])
        password.send_keys(credentials['linkedin']['password'])

        login_btn = self.driver.find_element(By.XPATH, "//button[@data-id='sign-in-form__submit-btn']")
        sleep(1)
        login_btn.click()

        url_linkedin = 'https://www.linkedin.com/jobs/search/?'

        if radius == 'any':
            radius = '25'

        url_linkedin = url_linkedin + f"distance={radius}&"

        if date_posted == '1':
            url_linkedin = url_linkedin + 'f_TPR=r86400&'
        elif date_posted == '7':
            url_linkedin = url_linkedin + 'f_TPR=r604800&'
        elif date_posted == '28':
            url_linkedin = url_linkedin + 'f_TPR=r2592000&'

        url_linkedin = (url_linkedin +
                        f"keywords={title}&location={location}&origin=JOB_SEARCH_PAGE_JOB_FILTER&refresh=true")

        self.driver.get(url_linkedin)
        sleep(2)

        self.scroll_down()
        found_jobs = self.get_jobs_on_page()

        scope = None
        scope_elements = self.driver.find_elements(By.XPATH, "//div[contains(@class, '__page-state')]")

        if len(scope_elements) > 0:
            # .text doesn't work here because element is hidden
            scope = scope_elements[0].get_attribute("textContent")
            scope = scope.split('of')
            scope = int(scope[1])

            for j in range(2, scope + 1):
                url_page = url_linkedin + f"&start={j * 25 - 25}"
                self.driver.get(url_page)
                sleep(3)
                self.scroll_down()
                new_found_jobs = self.get_jobs_on_page()
                found_jobs.extend(new_found_jobs)

        return found_jobs
