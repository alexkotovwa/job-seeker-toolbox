from selenium.webdriver.common.by import By
from features.classes.Base import Base


class Indeed(Base):

    def get_jobs_on_page(self):
        result_content = self.driver.find_elements(By.XPATH, "//td[@class='resultContent']")
        found = []
        for i in range(len(result_content)):
            job_title = result_content[i].find_element(By.XPATH, "./div/h2/a").text
            job_href = result_content[i].find_element(By.XPATH, "./div/h2/a").get_attribute('href')
            job_company = ""
            job_company_elements = result_content[i].find_elements(By.XPATH, "./div/div/span[@data-testid='company-name']")
            if len(job_company_elements) > 0:
                job_company = job_company_elements[0].text
            found.append([job_company, job_title, job_href])
        return found

    def get_jobs(self, title, location, radius, date_posted):

        url_base = f"https://www.indeed.com/jobs?q={title}&l={location}"
        url_ext = f"&radius={radius}&sort=date&fromage={date_posted}&start=0"
        pagination_btn_xpath = "//a[@data-testid='pagination-page-next']"

        if radius == 'any' and date_posted == 'any':
            url_ext = f"&sort=date&start=0"
        elif radius == 'any':
            url_ext = f"&sort=date&fromage={date_posted}&start=0"
        elif date_posted == 'any':
            url_ext = f"&radius={radius}&sort=date&start=0"

        url_indeed = url_base + url_ext

        found_jobs = []

        self.driver.get(url_indeed)
        for i in range(100):
            new_found_jobs = self.get_jobs_on_page()
            found_jobs.extend(new_found_jobs)
            pagination = self.driver.find_elements(By.XPATH, pagination_btn_xpath)
            if len(pagination) > 0:
                pagination[0].click()
                continue
            else:
                break

        return found_jobs
