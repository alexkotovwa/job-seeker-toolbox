import io
import json


class Base(object):

    def __init__(self, driver, *args, **kwargs):
        self.driver = driver

    @staticmethod
    def get_credentials(filename):
        with io.open(filename, "r", encoding="utf-8") as json_file:
            data = json.load(json_file)
            return data
