from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import datetime
import os


class Gmail(object):

    def __init__(self, scopes):
        self.SCOPES = scopes
        self.service = self.gmail_authenticate_behave()

    def gmail_authenticate_behave(self):
        creds = None
        # The file gmail_token.json stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('gmail_token.json'):
            creds = Credentials.from_authorized_user_file('gmail_token.json', self.SCOPES)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'gmail_credentials.json', self.SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('gmail_token.json', 'w') as token:
                token.write(creds.to_json())
        # self.service = build('gmail', 'v1', credentials=creds)
        # return self.service
        return build('gmail', 'v1', credentials=creds)

    def search_email_ids_by_messages(self, query):
        result = self.service.users().messages().list(userId='me', q=query).execute()
        email_ids = []
        if 'messages' in result:
            email_ids.extend(result['messages'])
        while 'nextPageToken' in result:
            page_token = result['nextPageToken']
            result = self.service.users().messages().list(userId='me', q=query, pageToken=page_token).execute()
            if 'messages' in result:
                email_ids.extend(result['messages'])
        return email_ids

    @staticmethod
    def get_value_from_headers(data, header_name):
        _headers = data['payload']['headers']
        for i in range(len(_headers)):
            if _headers[i]['name'] == header_name:
                return _headers[i]['value']
        return ''

    @staticmethod
    def get_date(data):
        epoch_time = str(data['internalDate'])
        epoch_time = int(epoch_time[0:len(epoch_time) - 3])
        return datetime.datetime.fromtimestamp(epoch_time)
