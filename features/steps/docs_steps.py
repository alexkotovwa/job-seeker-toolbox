from pyexcel_ods3 import get_data
from behave import given, when, then, step
from datetime import date
import webbrowser
from pyhtml2pdf import converter


@step('Docs API: Verify if "{query}" {condition} exist in ODS file')
def is_message_in_ods_file(context, query, condition):
    assert condition in ['should', 'should not'], f"The condition must be 'should' or 'should not'"

    if query == 'company':
        query = context.company

    file = context.path_to_ods

    data = get_data(file)
    companies = []
    for i in range(1, len(data['Sheet1']) - 1):
        company = data['Sheet1'][i]
        if len(company) > 0:
            companies.append(company[0].lower())

    if condition == 'should' and query.lower() not in companies:
        raise ValueError(f"{query} is not found in {file}")
    elif condition == 'should not' and query.lower() in companies:
        raise ValueError(f"{query} is found in {file}")
    elif condition == 'should' and query.lower() in companies:
        print(f"{query} is found in {file}")
    elif condition == 'should not' and query.lower() not in companies:
        print(f"{query} is not found in {file}")


@step('Docs API: Remove the positions that have been rejected from the list of found on "{source}"')
def update_found(context, source):
    assert source.lower() in ['indeed', 'linkedin'], f"The resource should be 'Indeed', 'LinkedIn'"

    list_before = []
    list_updated = []
    if source.lower() == 'indeed':
        list_before = context.indeed
    elif source.lower() == 'linkedin':
        list_before = context.linkedin
    else:
        # to do
        pass

    data = get_data(context.path_to_ods)
    rejected_companies = []
    for i in range(1, len(data['Sheet1']) - 1):
        company = data['Sheet1'][i]
        if len(company) > 0:
            rejected_companies.append(company[0].lower())

    rejected_companies = list(set(rejected_companies))
    rejected_companies.sort()
    for i in range(len(list_before)):
        company = str(list_before[i][0]).lower()

        if company not in rejected_companies:
            list_updated.append(list_before[i])

    if source.lower() == 'indeed':
        context.indeed = list_updated
    elif source.lower() == 'linkedin':
        context.linkedin = list_updated
    else:
        # to do
        pass


@step('Docs API: Create HTML file with the found positions on "{source}"')
def create_html(context, source):
    assert source.lower() in ['indeed', 'linkedin'], f"The resource should be 'Indeed', 'LinkedIn'"

    data, days, location, radius = [], '1', 'remote', '25'
    if source.lower() == 'indeed':
        data = context.indeed
        days = context.indeed_days
        location = str(context.indeed_location).replace('%20', ' ').replace('%2C', ' ')
        radius = context.indeed_radius
    elif source.lower() == 'linkedin':
        data = context.linkedin
        days = context.linkedin_days
        location = str(context.linkedin_location).replace('%20', ' ').replace('%2C', ' ')
        radius = context.linkedin_radius
    else:
        # to do
        pass

    today = date.today().strftime("%d-%m-%Y")
    file = context.path_to_html + f"Positions_for_applying_on_{source}_for_{today}.html"

    f = open(file, 'w', encoding='utf-8')

    html_begin = f""" 
    <html> 
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head> 
    <body style="padding: 20px;"> 
    <h2>Jobs posted on '{source}' within the past {days} days as of {today}</h2>
    <h4>Location: {location}</h4>
    <h4>Radius: {radius} miles</h4>
    <table class="table table-striped">
    <tbody>    
    """
    f.write(html_begin)
    if len(data) < 1:
        row = f"<p style='color:red;'>Data not found on '{source}'</p>"
        f.write(row)
    else:
        row = f"""<tr>
                  <th scope="col">#</th>
                  <th scope="col">Company</th>
                  <th scope="col">Job Title</th>
                  <th scope="col">Link</th>
                </tr>"""
        f.write(row)
        for i in range(len(data)):
            row = f"""<tr>
                      <th scope="row">{i + 1}</th>
                      <td>{data[i][0]}</td>
                      <td>{data[i][1]}</td>
                      <td><a href='{data[i][2]}'>Link</a></td>
                    </tr>"""
            f.write(row)

    html_close = """
    </tbody> 
    </table>  
    </body> 
    </html> 
    """
    f.write(html_close)
    f.close()

    context.last_created_html = file


@step('Docs API: Open created HTML file in the browser')
def open_html_file(context):
    webbrowser.open('file://' + context.last_created_html)


@step('Doc API: Generate the cover letter for the position "{position}" in company "{company}" for "{hiring_manager}"')
def make_cover_letter(context, position, company, hiring_manager):
    today = date.today().strftime("%m/%d/%Y")
    file_name = ('cover_letter_' + company.replace(' ', '_') + '_' + position.replace(' ', '_') + '_' +
                 today.replace('/', '_'))
    file_html = context.path_to_html + file_name + '.html'
    file_pdf = context.path_to_pdf + file_name + '.pdf'

    with open(context.cover_template) as ft:
        letter = ft.read()

    letter = (letter.replace('{today}', today).replace('{position}', position).
              replace('{company}', company).replace('{hiring_manager}', hiring_manager))

    f = open(file_html, 'w')
    f.write(letter)
    f.close()

    context.last_created_html = file_html
    converter.convert(f"file:///{file_html}", file_pdf)
    context.last_created_pdf = file_pdf


@step('Docs API: Open created PDF file in the browser')
def open_html_file(context):
    webbrowser.open('file://' + context.last_created_pdf)


@step('Docs API: Create HTML file with confirmations')
def create_html(context):
    confirmations = context.confirmations

    file = context.path_to_html + f"Confirmations.html"

    f = open(file, 'w', encoding='utf-8')

    html_begin = f""" 
    <html> 
    <head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script>
        var tooltip, hidetooltiptimer
        function getSelectionText(){{
            var selectedText = ""
            if (window.getSelection){{
                selectedText = window.getSelection().toString()
            }}
            return selectedText
        }}
    
        function copySelectionText(){{
            var copysuccess
            try{{
                copysuccess = document.execCommand("copy")
            }} catch(e){{
                copysuccess = false
            }}
        return copysuccess
        }}
    
        function createtooltip(){{
            tooltip = document.createElement('div')
            tooltip.style.cssText = 
                'position:absolute; background:black; color:white; padding:4px;z-index:10000;'
                + 'border-radius:2px; font-size:12px;box-shadow:3px 3px 3px rgba(0,0,0,.4);'
                + 'opacity:0;transition:opacity 0.3s'
            tooltip.innerHTML = 'Copied into clipboard!'
            document.body.appendChild(tooltip)
        }}
    
        function showtooltip(e){{
            var evt = e || event
            clearTimeout(hidetooltiptimer)
            tooltip.style.left = evt.pageX - 10 + 'px'
            tooltip.style.top = evt.pageY + 15 + 'px'
            tooltip.style.opacity = 1
            hidetooltiptimer = setTimeout(function(){{
                tooltip.style.opacity = 0
            }}, 800)
        }}
    
    </script>
    </head> 
    <body style="padding: 20px;"> 
    <h2>Confirmations</h2>
    <p id="control" style="border:1px solid orange;background:lightyellow;padding:5px">You will see the 
    copied text here for your review</p>
    <div id="workplace">
    <table class="table table-striped">
    <tbody>    
    """
    f.write(html_begin)
    if len(confirmations) < 1:
        row = f"<p style='color:red;'>No confirmations were found</p>"
        f.write(row)
    else:
        row = f"""<tr>
                    <th scope="col">#</th>
                    <th scope="col">Date</th>
                    <th scope="col">From</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Message</th>
                </tr>"""
        f.write(row)
        for i in range(len(confirmations)):
            r = confirmations[i]
            _from = r[1].replace('<', '(').replace('>', ')')
            row = f"""<tr>
                      <th scope="row">{i + 1}</th>
                      <td>{r[0]}</td>
                      <td>{_from}</td>
                      <td>{r[2]}</td>
                      <td>{r[3]}</td>
                    </tr>"""
            f.write(row)

    html_close = """
    </div>
    <script>
        createtooltip()
        var control = document.getElementById('control')
        var buddhaquote = document.getElementById('workplace')
        buddhaquote.addEventListener('mouseup', function(e){
            var selected = getSelectionText()
            if (selected.length > 0){
                var copysuccess = copySelectionText()
                showtooltip(e)
                control.innerHTML = selected
            }
        }, false)
    </script>
    </tbody> 
    </table>  
    </body> 
    </html> 
    """
    f.write(html_close)
    f.close()

    context.last_created_html = file
