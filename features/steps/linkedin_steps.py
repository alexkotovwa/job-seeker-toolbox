from behave import given, when, then, step
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from features.classes.Linkedin import *


@step('LinkedIn: Find "{title}" in location "{location}" within "{radius}" miles for the last "{date_posted}" days')
def open_url(context, title, location, radius, date_posted):
    assert radius in ['0', '5', '10', '25', '50', '100', 'any'], (f"The miles should be one of '0', '5', '10', '15', "
                                                                  f"'25', '50', '100' or 'any'.")
    assert date_posted in ['1', '7', '28', 'any'], f"The days should be one of '1', '7', '28' or 'any'."

    context.driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    wait_time = 10
    context.driver.implicitly_wait(wait_time)
    wait = WebDriverWait(context.driver, wait_time)

    title = title.replace(' ', '%20')
    location = location.replace(' ', '%20').replace(',', '%2C')

    my_linkedin = Linkedin(context.driver)
    credentials = my_linkedin.get_credentials('credentials.json')

    found_jobs = my_linkedin.get_jobs(credentials, title, location, radius, date_posted)

    context.linkedin = found_jobs
    context.linkedin_days = date_posted
    context.linkedin_location = location
    context.linkedin_radius = radius
