from behave import given, when, then, step
from googleapiclient.errors import HttpError
from features.classes.Gmail import *

# If modifying these scopes, delete the file gmail_token.json.
SCOPES = ['https://mail.google.com/']


@step('Gmail API: Verify if "{query}" {condition} exist in emails')
def is_message_in_email(context, query, condition):
    assert condition in ['should', 'should not'], f"The condition must be 'should' or 'should not'"
    if query == 'company':
        query = context.company

    try:
        my_gmail = Gmail(SCOPES)
        email_ids = my_gmail.search_email_ids_by_messages(query=context.gmail_label + ' ' + query)
        if email_ids and condition == 'should':
            print(email_ids)

            for i in range(len(email_ids)):
                email = my_gmail.service.users().messages().get(userId='me', id=email_ids[i]['id'],
                                                                format='full').execute()
                print(f"--- {i + 1} ---\n" + email['snippet'])

        elif email_ids and condition == 'should not':
            raise ValueError(f"I have already interacted with {query}")

        elif not email_ids and condition == 'should not':
            print(f"I have never interacted with {query}")

    except HttpError as error:
        # TODO(developer) - Handle errors from gmail API.
        print(f'An error occurred: {error}')


@step('Gmail API: Remove the positions that have already been applied for from the list of found on "{source}"')
def update_found(context, source):
    assert source.lower() in ['indeed', 'linkedin'], f"The resource should be 'Indeed', 'LinkedIn'"

    list_before = []
    list_updated = []
    if source.lower() == 'indeed':
        list_before = context.indeed
    elif source.lower() == 'linkedin':
        list_before = context.linkedin
    else:
        # to do
        pass

    my_gmail = Gmail(SCOPES)

    for i in range(len(list_before)):
        company = str(list_before[i][0]).lower()
        email_ids = my_gmail.search_email_ids_by_messages(query=context.gmail_label + ' ' + company)
        if not email_ids:
            list_updated.append(list_before[i])

    if source.lower() == 'indeed':
        context.indeed = list_updated
    elif source.lower() == 'linkedin':
        context.linkedin = list_updated
    else:
        # to do
        pass


@step('Gmail API: Get a list of application confirmations from "{source}"')
def get_confirmations(context, source):
    confirmations = []
    label = context.gmail_label
    if source.lower() == 'indeed':
        label = label + ' ' + 'indeedapply@indeed.com'
    elif source.lower() == 'amazon':
        label = label + ' ' + 'noreply@mail.amazon.jobs'
    elif source.lower() == 'worksourcewa':
        label = label + ' ' + 'WorkSourceWashingtonNoReply@esd.wa.gov'
    elif source.lower() == 'linkedin':
        label = label + ' ' + 'jobs-noreply@linkedin.com'
    elif 'any' not in source.lower():
        label = label + ' ' + source.lower()

    my_gmail = Gmail(SCOPES)
    email_ids = my_gmail.search_email_ids_by_messages(query=label)

    for i in range(len(email_ids)):
        email = my_gmail.service.users().messages().get(userId='me', id=email_ids[i]['id'], format='full').execute()

        _date = my_gmail.get_date(email)
        email_date = f"{_date.month}/{_date.day}/{_date.year}"
        email_from = my_gmail.get_value_from_headers(email, 'From')
        email_subject = my_gmail.get_value_from_headers(email, 'Subject')
        email_snippet = str(email['snippet']).replace('&#39;', '\'').replace('&amp;', '&')

        confirmations.append([email_date, email_from, email_subject, email_snippet])

    context.confirmations = confirmations
