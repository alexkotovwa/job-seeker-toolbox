from behave import given, when, then, step
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from features.classes.Indeed import *


@step('Indeed: Find "{title}" in location "{location}" within "{radius}" miles for the last "{date_posted}" days')
def open_url(context, title, location, radius, date_posted):
    assert radius in ['0', '5', '10', '15', '25', '35', '50', '100',
                      'any'], f"The miles should be one of '0', '5', '10', '15', '25', '35', '50', '100' or 'any'."
    assert date_posted in ['1', '3', '7', '14', 'any'], f"The days should be one of '1', '3', '7', '14' or 'any'."

    context.driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))
    wait_time = 10
    context.driver.implicitly_wait(wait_time)
    wait = WebDriverWait(context.driver, wait_time)

    title = title.replace(' ', '%20')
    location = location.replace(' ', '%20').replace(',', '%2C')

    my_indeed = Indeed(context.driver)
    found_jobs = my_indeed.get_jobs(title, location, radius, date_posted)

    context.indeed = found_jobs
    context.indeed_days = date_posted
    context.indeed_location = location
    context.indeed_radius = radius
